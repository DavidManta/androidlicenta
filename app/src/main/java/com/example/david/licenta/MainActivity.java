package com.example.david.licenta;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.david.licenta.fragments.AdaugaFragment;
import com.example.david.licenta.fragments.CategoriiFragment;
import com.example.david.licenta.fragments.ContFragment;
import com.example.david.licenta.fragments.FavoriteFragment;
import com.example.david.licenta.fragments.MesajeFragment;
import com.example.david.licenta.fragments.RegistrationFragment;
import com.parse.Parse;


public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_categorii:
                    transaction.replace(R.id.content, new CategoriiFragment()).commit();
                    return true;
                case R.id.navigation_favorite:
                    transaction.replace(R.id.content, new FavoriteFragment()).commit();
                    return true;
                case R.id.navigation_adauga:
                    transaction.replace(R.id.content, AdaugaFragment.getInstance()).commit();
                    return true;
                case R.id.navigation_mesaje:
                    transaction.replace(R.id.content, new MesajeFragment()).commit();
                    return true;
                case R.id.navigation_cont:
                    if (ContFragment.getVersiune() == 0) {
                        transaction.replace(R.id.content, ContFragment.getInstance()).commit();
                    } else {
                        transaction.replace(R.id.content, RegistrationFragment.getInstance()).commit();
                    }
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
