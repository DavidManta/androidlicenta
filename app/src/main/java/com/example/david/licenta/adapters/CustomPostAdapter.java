package com.example.david.licenta.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.database.DatabaseHandler;
import com.example.david.licenta.liste.ListaFavorite;
import com.example.david.licenta.R;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.models.AnuntDetaliat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomPostAdapter extends BaseAdapter {

    private List<Anunt> listaAnunturi;
    private Context ctxt;
    private DatabaseHandler databaseHandler;

    public CustomPostAdapter(Context context, List<Anunt> posts) {

        this.ctxt = context;
        this.listaAnunturi = posts;
    }

    @Override
    public int getCount() {
        return listaAnunturi.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAnunturi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Anunt post = listaAnunturi.get(position);
        ViewHolder holder = new ViewHolder();

        if (null == convertView) {
            LayoutInflater inflater = (LayoutInflater) ctxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_element_lista_anunt, null);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        databaseHandler = new DatabaseHandler(ctxt, null, null, 1);
        holder.titlu = (TextView) convertView.findViewById(R.id.tvTitlu);
        holder.descriere = (TextView) convertView.findViewById(R.id.tvDescriere);
        holder.pret = (TextView) convertView.findViewById(R.id.tvPostPret);

        holder.picture = (ImageView) convertView.findViewById(R.id.postPicture);

        Picasso.with(ctxt).load(listaAnunturi.get(position).getPictureUrl()).resize(60, 60).centerCrop().into(holder.picture);

        holder.titlu.setText(post.getTitlu());
        holder.descriere.setText(post.getDescriere());
        holder.pret.setText(post.getPret() + " " + post.getCurrency());

        holder.favorite = (ImageButton) convertView.findViewById(R.id.setAsFavouriteBtn);
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListaFavorite.getLista().add(listaAnunturi.get(position));
                databaseHandler.addAnunt(listaAnunturi.get(position));
                Toast.makeText(view.getContext(), "Anuntul " + position + " a fost adaugat la favorite", Toast.LENGTH_SHORT).show();
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctxt, AnuntDetaliat.class);
                intent.putExtra("Anunt", listaAnunturi.get(position));
                ctxt.startActivity(intent);
                Toast.makeText(view.getContext(), "Detalii anuntul " + position, Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView titlu;
        TextView descriere;
        TextView pret;
        ImageButton favorite;
        ImageView picture;
    }


}
