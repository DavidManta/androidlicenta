package com.example.david.licenta.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.R;
import com.example.david.licenta.database.DatabaseHandler;
import com.example.david.licenta.fragments.AdaugaFragment;
import com.example.david.licenta.liste.ListaAnunturi;
import com.example.david.licenta.liste.ListaAnunturileMele;
import com.example.david.licenta.liste.ListaFavorite;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.models.AnuntDetaliat;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by David on 3/18/2018.
 */

public class EditPostAdapter extends BaseAdapter {

    private List<Anunt> listaAnunturi;
    private Context ctxt;


    public EditPostAdapter(Context context, List<Anunt> posts) {

        this.ctxt = context;
        this.listaAnunturi = posts;
    }

    @Override
    public int getCount() {
        return listaAnunturi.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAnunturi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) ctxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_element_editabil, null);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.titlu = (TextView) convertView.findViewById(R.id.tvTitlu);
        holder.descriere = (TextView) convertView.findViewById(R.id.tvDescriere);
        holder.pret = (TextView) convertView.findViewById(R.id.tvPostPret);

        holder.picture = (ImageView) convertView.findViewById(R.id.postPicture);

        Picasso.with(ctxt).load(listaAnunturi.get(position).getPictureUrl()).resize(60, 60).centerCrop().into(holder.picture);

        holder.titlu.setText(ListaAnunturileMele.getLista().get(position).getTitlu());
        holder.descriere.setText(ListaAnunturileMele.getLista().get(position).getDescriere());
        holder.pret.setText(ListaAnunturileMele.getLista().get(position).getPret() + " " + listaAnunturi.get(position).getCurrency());

        holder.edit = (ImageButton) convertView.findViewById(R.id.editBtn);

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                new AlertDialog.Builder(view.getContext())
                        .setTitle("Delete")
                        .setMessage("Ești sigur ca vrei să ștergi anunțul?")
                        .setNegativeButton("Nu", null)
                        .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Anunt");
                                query.whereEqualTo("titlu", listaAnunturi.get(position).getTitlu());
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> objects, ParseException e) {
                                        if (e == null) {
                                            for (int i = 0; i < objects.size(); i++) {
                                                objects.get(i).deleteInBackground();
                                                objects.get(i).saveInBackground();
                                            }
                                        }
                                    }
                                });
                                Toast.makeText(view.getContext(), "Anuntul " + position + " a fost sters!", Toast.LENGTH_SHORT).show();
                                EditPostAdapter.this.listaAnunturi.remove(position);
                                notifyDataSetChanged();
                            }
                        }).create().show();
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctxt, AnuntDetaliat.class);
                intent.putExtra("Anunt", listaAnunturi.get(position));
                ctxt.startActivity(intent);
                Toast.makeText(view.getContext(), "Detalii anuntul " + position, Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;

    }

    static class ViewHolder {
        TextView titlu;
        TextView descriere;
        TextView pret;
        ImageButton edit;
        ImageView picture;

    }


}
