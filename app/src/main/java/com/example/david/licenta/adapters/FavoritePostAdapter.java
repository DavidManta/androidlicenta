package com.example.david.licenta.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.R;
import com.example.david.licenta.database.DatabaseHandler;
import com.example.david.licenta.liste.ListaFavorite;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.models.AnuntDetaliat;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by David on 3/3/2018.
 */

public class FavoritePostAdapter extends BaseAdapter {

    private List<Anunt> listaAnunturi;
    private Context ctxt;
    private DatabaseHandler databaseHandler;


    public FavoritePostAdapter(Context context, List<Anunt> posts) {

        this.ctxt = context;
        this.listaAnunturi = posts;
    }

    @Override
    public int getCount() {
        return listaAnunturi.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAnunturi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaAnunturi.indexOf(listaAnunturi.get(position));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();
        databaseHandler=new DatabaseHandler(ctxt,null,null,1);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) ctxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_element_favorit_anunt, null);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.titlu = (TextView) convertView.findViewById(R.id.tvTitlu);
        holder.descriere = (TextView) convertView.findViewById(R.id.tvDescriere);
        holder.pret = (TextView) convertView.findViewById(R.id.tvPostPret);

        holder.picture = (ImageView) convertView.findViewById(R.id.postPicture);

        Picasso.with(ctxt).load(listaAnunturi.get(position).getPictureUrl()).resize(60, 60).centerCrop().into(holder.picture);

        holder.titlu.setText(ListaFavorite.getLista().get(position).getTitlu());
        holder.descriere.setText(ListaFavorite.getLista().get(position).getDescriere());
        holder.pret.setText(ListaFavorite.getLista().get(position).getPret() + " " + ListaFavorite.getLista().get(position).getCurrency());

        holder.remove = (ImageButton) convertView.findViewById(R.id.removeBtn);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHandler.deleteAnunt(listaAnunturi.get(position).getTitlu());
                FavoritePostAdapter.this.listaAnunturi.remove(position);
                notifyDataSetChanged();
                Toast.makeText(view.getContext(), "Anuntul " + position + " a fost sters de la favorite", Toast.LENGTH_SHORT).show();
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctxt, AnuntDetaliat.class);
                intent.putExtra("Anunt", listaAnunturi.get(position));
                ctxt.startActivity(intent);
                Toast.makeText(view.getContext(), "Detalii anuntul " + position, Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;

    }

    static class ViewHolder {
        TextView titlu;
        TextView descriere;
        TextView pret;
        ImageButton remove;
        ImageView picture;

    }


}


