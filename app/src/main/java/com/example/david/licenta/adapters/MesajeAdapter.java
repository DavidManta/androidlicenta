package com.example.david.licenta.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.R;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.models.AnuntDetaliat;
import com.example.david.licenta.models.Mesaj;
import com.example.david.licenta.models.MesajDetaliat;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MesajeAdapter extends BaseAdapter {


    private List<Mesaj> listaMesaje;
    private Context ctxt;

    public MesajeAdapter(Context context, List<Mesaj> mesaje) {
        this.ctxt = context;
        this.listaMesaje = mesaje;
    }

    @Override
    public int getCount() {
        return listaMesaje.size();
    }

    @Override
    public Object getItem(int position) {
        return listaMesaje.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        Mesaj post = listaMesaje.get(position);

        if (null == view) {

            view = LayoutInflater.from(ctxt).inflate(R.layout.activity_element_mesaj, parent, false);

            TextView autor = (TextView) view.findViewById(R.id.tvAutor);
            TextView continut = (TextView) view.findViewById(R.id.tvContinut);
            TextView data = (TextView) view.findViewById(R.id.tvData);

            autor.setText(post.getAutor());
            continut.setText(post.getContinut());
            data.setText(post.getDate());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ctxt, MesajDetaliat.class);
                    intent.putExtra("Mesaj",listaMesaje.get(position));
                    ctxt.startActivity(intent);
                    Toast.makeText(view.getContext(), "Detalii mesaj " + position, Toast.LENGTH_SHORT).show();
                }
            });

        }

        return view;
    }
}
