package com.example.david.licenta.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.example.david.licenta.liste.ListaFavorite;
import com.example.david.licenta.models.Anunt;

/**
 * Created by David on 3/18/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "anunturi.db";
    private static final String TABLE_NAME = "anunturi";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_TITLU = "titluAnunt";
    private static final String COLUMN_DESCRIERE = "descriereAnunt";
    private static final String COLUMN_PRET = "pretAnunt";
    private static final String COLUMN_CURRENCY = "currencyAnunt";
    private static final String COLUMN_EMAIL = "emailAnunt";
    private static final String COLUMN_URL = "anuntUrl";
    private static final String COLUMN_TELEFON = "anuntTelefon";
    private static final String COLUMN_CATEGORIE = "categorieAnunt";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TITLU + " TEXT, " +
                COLUMN_DESCRIERE + " TEXT, " +
                COLUMN_CATEGORIE + " TEXT, " +
                COLUMN_EMAIL + " TEXT, " +
                COLUMN_PRET + " TEXT, " +
                COLUMN_CURRENCY + " TEXT, " +
                COLUMN_URL + " TEXT, " +
                COLUMN_TELEFON + " TEXT " + ");";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    @SuppressLint("StaticFieldLeak")
    public void addAnunt(final Anunt anunt) {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_TITLU,anunt.getTitlu());
                contentValues.put(COLUMN_CATEGORIE,anunt.getCategorie());
                contentValues.put(COLUMN_DESCRIERE,anunt.getDescriere());
                contentValues.put(COLUMN_EMAIL,anunt.getEmail());
                contentValues.put(COLUMN_URL,anunt.getPictureUrl());
                contentValues.put(COLUMN_PRET,anunt.getPret());
                contentValues.put(COLUMN_CURRENCY,anunt.getCurrency());
                contentValues.put(COLUMN_TELEFON,anunt.getNrTelefon());
                SQLiteDatabase db = getWritableDatabase();
                db.insert(TABLE_NAME,null,contentValues);
                db.close();
                return null;
            }
        }.execute();
    }

    public void deleteAnunt(String titlu) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE "
                + COLUMN_TITLU + " = '" + titlu + "'";
        db.execSQL(query);
        db.close();
    }

    @SuppressLint("StaticFieldLeak")
    public  void obtineToateIncidentele() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try
                {
                    SQLiteDatabase db = getReadableDatabase();
                    Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
                    if (cursor != null)
                        while (cursor.moveToNext()) {
                            int indexTitlu = cursor.getColumnIndex(COLUMN_TITLU);
                            int indexCategorie = cursor.getColumnIndex(COLUMN_CATEGORIE);
                            int indexDescriere = cursor.getColumnIndex(COLUMN_DESCRIERE);
                            int indexEmail = cursor.getColumnIndex(COLUMN_EMAIL);
                            int indexURL = cursor.getColumnIndex(COLUMN_URL);
                            int indexPret = cursor.getColumnIndex(COLUMN_PRET);
                            int indexCurrency = cursor.getColumnIndex(COLUMN_CURRENCY);
                            int indexTelefon = cursor.getColumnIndex(COLUMN_TELEFON);
                            ListaFavorite.getLista().add(new Anunt(
                                    cursor.getString(indexTitlu), cursor.getString(indexCategorie), cursor.getString(indexDescriere),
                                    cursor.getString(indexEmail), cursor.getString(indexPret), cursor.getString(indexCurrency), cursor.getString(indexURL),
                                    cursor.getString(indexTelefon)
                            ));
                        }
                    db.close();
                } catch(Exception ex)
                {
                    ex.printStackTrace();
                }
            return null;
            }
        }.execute();
    }
}
