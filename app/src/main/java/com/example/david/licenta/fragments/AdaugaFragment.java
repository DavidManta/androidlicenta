package com.example.david.licenta.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.liste.ListaAnunturileMele;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.liste.ListaAnunturi;
import com.example.david.licenta.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.parse.ParseObject;


public class AdaugaFragment extends Fragment implements View.OnClickListener {


    private static AdaugaFragment instance;
    private Button adaugaBtn;
    private EditText etTitlu;
    private EditText etTelefon;
    private EditText etDescriere;
    private EditText etUrl;
    private Spinner sp_status;
    private Spinner sp_categorie;
    private EditText etPret;
    private FirebaseAuth firebaseAuth;

    public static AdaugaFragment getInstance() {
        if (instance == null)
            instance = new AdaugaFragment();
        return instance;
    }

    public AdaugaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_adauga, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        adaugaBtn = (Button) getView().findViewById(R.id.bt_adauga);
        adaugaBtn.setOnClickListener(this);
        etTitlu = (EditText) getView().findViewById(R.id.et_titlu);
        etTelefon = (EditText) getView().findViewById(R.id.et_telefon);
        etDescriere = (EditText) getView().findViewById(R.id.et_descriere);
        sp_status = (Spinner) getView().findViewById(R.id.spinner_status);
        sp_categorie = (Spinner) getView().findViewById(R.id.spinner_categorie);
        etUrl = (EditText) getView().findViewById(R.id.et_Url);
        etPret = (EditText) getView().findViewById(R.id.et_pret);
        firebaseAuth = FirebaseAuth.getInstance();


        super.onViewCreated(view, savedInstanceState);
    }

    private void adaugare() {

        String titlu = etTitlu.getText().toString();
        String descriere = etDescriere.getText().toString();
        String telefon = etTelefon.getText().toString();
        String pret = etPret.getText().toString();
        String currency = sp_status.getSelectedItem().toString();
        String categorie = sp_categorie.getSelectedItem().toString();
        String urlPoza = etUrl.getText().toString();

        if (firebaseAuth.getCurrentUser() != null) {
            if (titlu.length() > 0 && descriere.length() > 0 && telefon.length() > 0 && urlPoza.length() > 0) {
                String email = firebaseAuth.getCurrentUser().getEmail();
                Anunt anunt = new Anunt(titlu, categorie, descriere, email, pret, currency, urlPoza, telefon);

                //Baza de date online
                ParseObject parseObject = new ParseObject("Anunt");
                parseObject.put("titlu", anunt.getTitlu());
                parseObject.put("categorie", anunt.getCategorie());
                parseObject.put("descriere", anunt.getDescriere());
                parseObject.put("email", anunt.getEmail());
                parseObject.put("pret", anunt.getPret());
                parseObject.put("currency", anunt.getCurrency());
                parseObject.put("urlPoza", anunt.getPictureUrl());
                parseObject.put("telefon", anunt.getNrTelefon());
                parseObject.saveInBackground();

                Toast.makeText(getActivity(), "Anunțul a fost adăugat!", Toast.LENGTH_SHORT).show();
                etTitlu.setText(null);
                etDescriere.setText(null);
                etTelefon.setText(null);
                etUrl.setText(null);
                etPret.setText(null);
            } else {
                Toast.makeText(getActivity(), "Nu ai completat tot formularul", Toast.LENGTH_SHORT).show();
            }
        } else {
            AlertDialog.Builder myAlert = new AlertDialog.Builder(getContext());
            myAlert.setMessage("Pentru a adăuga un anunț trebuie sa fii logat.")
                    .setTitle("Nu ești logat!")
                    .setPositiveButton("Ok", null);
            myAlert.show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == adaugaBtn) {
            adaugare();

        }

    }


}
