package com.example.david.licenta.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.david.licenta.liste.ListaAnunturileMele;
import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.adapters.CustomPostAdapter;
import com.example.david.licenta.liste.ListaAnunturi;
import com.example.david.licenta.R;
import com.google.firebase.auth.FirebaseAuth;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class CategoriiFragment extends Fragment {

    ListView lvAnunturi = null;
    public CustomPostAdapter customPostAdapter;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private static boolean alreadyExecuted = false;

    public CategoriiFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ListaAnunturi.getLista().clear();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Anunt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        Object object = objects.get(i);
                        String titlu = ((ParseObject) object).getString("titlu");
                        String categorie = ((ParseObject) object).getString("categorie");
                        String descriere = ((ParseObject) object).getString("descriere");
                        String email = ((ParseObject) object).getString("email");
                        String pret = ((ParseObject) object).getString("pret");
                        String currency = ((ParseObject) object).getString("currency");
                        String picUrl = ((ParseObject) object).getString("urlPoza");
                        String nrTelefon = ((ParseObject) object).getString("telefon");
                        Anunt anunt = new Anunt(titlu, categorie, descriere, email, pret, currency, picUrl, nrTelefon);
                        ListaAnunturi.getLista().add(anunt);

                        firebaseAuth = FirebaseAuth.getInstance();
                        if (firebaseAuth.getCurrentUser() != null) {
                            if (anunt.getEmail().equals(firebaseAuth.getCurrentUser().getEmail()) && !alreadyExecuted) {
                                ListaAnunturileMele.getLista().add(anunt);
                            }
                        }
                    }
                    alreadyExecuted = true;
                }
                progressDialog.dismiss();
                customPostAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_categorii, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvAnunturi = (ListView) getView().findViewById(R.id.lvAnunturi);
        customPostAdapter = new CustomPostAdapter(getContext(), ListaAnunturi.getLista());
        lvAnunturi.setAdapter(customPostAdapter);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s != null && !s.isEmpty()) {
                    ArrayList<Anunt> newList = new ArrayList<>();
                    for (Anunt anunt : ListaAnunturi.getLista()) {
                        if (anunt.getTitlu().toLowerCase().contains(s))
                            newList.add(anunt);
                    }
                    customPostAdapter = new CustomPostAdapter(getContext(), newList);
                    lvAnunturi.setAdapter(customPostAdapter);
                } else {
                    customPostAdapter = new CustomPostAdapter(getContext(), ListaAnunturi.getLista());
                    lvAnunturi.setAdapter(customPostAdapter);
                }
                return true;
            }
        });
    }

}





