package com.example.david.licenta.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.david.licenta.database.DatabaseHandler;
import com.example.david.licenta.liste.ListaFavorite;
import com.example.david.licenta.R;
import com.example.david.licenta.adapters.FavoritePostAdapter;


public class FavoriteFragment extends Fragment {

    private ListView listView = null;
    private DatabaseHandler databaseHandler;
    private FavoritePostAdapter adapter;
    private static boolean alreadyExecuted = false;


    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!alreadyExecuted) {
            ListaFavorite.getLista().clear();
            databaseHandler = new DatabaseHandler(getContext(), null, null, 1);
            databaseHandler.obtineToateIncidentele();
            alreadyExecuted = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorite, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) getView().findViewById(R.id.lvFavorite);
        adapter = new FavoritePostAdapter(getContext(), ListaFavorite.getLista());
        listView.setAdapter(adapter);

    }


}
