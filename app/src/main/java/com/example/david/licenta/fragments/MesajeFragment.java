package com.example.david.licenta.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.david.licenta.R;
import com.example.david.licenta.adapters.MesajeAdapter;
import com.example.david.licenta.liste.ListaMesaje;
import com.example.david.licenta.models.CreareMesaj;
import com.example.david.licenta.models.Mesaj;
import com.google.firebase.auth.FirebaseAuth;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


public class MesajeFragment extends Fragment {

    private ProgressDialog progressDialog;
    private ListView lvMesaje = null;
    private MesajeAdapter mesajeAdapter;
    private FirebaseAuth firebaseAuth;


    public MesajeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        ListaMesaje.getLista().clear();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Mesaj");
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        for (int i = 0; i < objects.size(); i++) {
                            Object object = objects.get(i);
                            String autor = ((ParseObject) object).getString("autor");
                            String continut = ((ParseObject) object).getString("continut");
                            String destinatar = ((ParseObject) object).getString("destinatar");
                            String data = ((ParseObject) object).getString("data");
                            Mesaj mesaj = new Mesaj(autor, continut, destinatar, data);
                            if (mesaj.getDestinatar().equals(firebaseAuth.getCurrentUser().getEmail())) {
                                ListaMesaje.getLista().add(mesaj);
                                mesajeAdapter.notifyDataSetChanged();
                            }

                        }
                        progressDialog.dismiss();
                    }
                }
            });
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Nu esti logat!")
                    .setMessage("Pentru a trimite mesaje trebuie să fii logat.")
                    .setNegativeButton("Ok",null).create().show();
            progressDialog.dismiss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_mesaje, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lvMesaje = (ListView) getView().findViewById(R.id.lvMesaje);
        mesajeAdapter = new MesajeAdapter(getContext(), ListaMesaje.getLista());
        lvMesaje.setAdapter(mesajeAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.plus, menu);
        MenuItem item = menu.findItem(R.id.app_bar_plus);

        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(getContext(), CreareMesaj.class);
                startActivity(intent);
                return true;
            }
        });


    }
}

