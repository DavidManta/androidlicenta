package com.example.david.licenta.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.david.licenta.R;
import com.example.david.licenta.liste.ListaAnunturi;
import com.example.david.licenta.liste.ListaAnunturileMele;
import com.example.david.licenta.adapters.EditPostAdapter;
import com.example.david.licenta.models.Anunt;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;


public class ProfileFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    private TextView textViewUserEmail;
    private Button buttonLogout;
    private ListView listView;
    private EditPostAdapter customPostAdapter;
    private ProgressDialog progressDialog;
    private static boolean alreadyExecuted = false;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        ListaAnunturileMele.getLista().clear();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Anunt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        Object object = objects.get(i);
                        String titlu = ((ParseObject) object).getString("titlu");
                        String categorie = ((ParseObject) object).getString("categorie");
                        String descriere = ((ParseObject) object).getString("descriere");
                        String email = ((ParseObject) object).getString("email");
                        String pret = ((ParseObject) object).getString("pret");
                        String currency = ((ParseObject) object).getString("currency");
                        String picUrl = ((ParseObject) object).getString("urlPoza");
                        String nrTelefon = ((ParseObject) object).getString("telefon");
                        Anunt anunt = new Anunt(titlu, categorie, descriere, email, pret, currency, picUrl, nrTelefon);
                        firebaseAuth = FirebaseAuth.getInstance();
                        if (firebaseAuth.getCurrentUser() != null) {
                            if (anunt.getEmail().equals(firebaseAuth.getCurrentUser().getEmail()) && !alreadyExecuted) {
                                ListaAnunturileMele.getLista().add(anunt);
                            }
                        }
                    }
                }
                progressDialog.dismiss();
                customPostAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            Fragment nf = new ContFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            transaction.replace(R.id.content, nf);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();

        textViewUserEmail = (TextView) getView().findViewById(R.id.tvUserEmail);
        buttonLogout = (Button) getView().findViewById(R.id.logoutBtn);

        listView=(ListView)getView().findViewById(R.id.lvAnunturileMele);
        customPostAdapter=new EditPostAdapter(getContext(), ListaAnunturileMele.getLista());
        listView.setAdapter(customPostAdapter);

        textViewUserEmail.setText("Welcome " + user.getEmail());

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
                Fragment nf = new ContFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                transaction.replace(R.id.content, nf);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}
