package com.example.david.licenta.liste;

import com.example.david.licenta.models.Anunt;
import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by David on 12/20/2017.
 */

public class ListaAnunturi {
    private static ArrayList<Anunt> lista = new ArrayList<>();

    public static ArrayList<Anunt> getLista() {
        return lista;
    }

    private ListaAnunturi() {

    }
}
