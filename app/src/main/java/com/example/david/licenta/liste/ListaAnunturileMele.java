package com.example.david.licenta.liste;

import com.example.david.licenta.models.Anunt;

import java.util.ArrayList;

/**
 * Created by David on 3/18/2018.
 */

public class ListaAnunturileMele {
    private static ArrayList<Anunt> lista = new ArrayList<>();

    public static ArrayList<Anunt> getLista() {
        return lista;
    }

    private ListaAnunturileMele() {

    }
}