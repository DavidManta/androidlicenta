package com.example.david.licenta.liste;

import com.example.david.licenta.models.Anunt;

import java.util.ArrayList;

/**
 * Created by David on 3/3/2018.
 */

public class ListaFavorite {
    private static ArrayList<Anunt> lista = new ArrayList<>();

    public static ArrayList<Anunt> getLista() {
        return lista;
    }

    private ListaFavorite() {

    }
}
