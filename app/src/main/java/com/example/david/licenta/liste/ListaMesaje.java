package com.example.david.licenta.liste;

import com.example.david.licenta.models.Anunt;
import com.example.david.licenta.models.Mesaj;

import java.util.ArrayList;

public class ListaMesaje {
    private static ArrayList<Mesaj> lista = new ArrayList<>();

    public static ArrayList<Mesaj> getLista() {
        return lista;
    }

    private ListaMesaje() {

    }
}
