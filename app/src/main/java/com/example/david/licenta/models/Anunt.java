package com.example.david.licenta.models;

import java.io.Serializable;

/**
 * Created by David on 12/13/2017.
 */

public class Anunt implements Serializable {
    private String titlu;
    private String categorie;
    private String descriere;
    private String email;
    private String pret;
    private String pictureUrl;
    private String nrTelefon;
    private String currency;

    public Anunt(String titlu, String categorie, String descriere, String email, String pret, String currency, String pictureUrl, String nrTelefon) {
        this.titlu = titlu;
        this.categorie = categorie;
        this.descriere = descriere;
        this.email = email;
        this.pret = pret;
        this.currency = currency;
        this.pictureUrl = pictureUrl;
        this.nrTelefon = nrTelefon;
    }


    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPret() {
        return pret;
    }

    public void setPret(String pret) {
        this.pret = pret;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getNrTelefon() {
        return nrTelefon;
    }

    public void setNrTelefon(String nrTelefon) {
        this.nrTelefon = nrTelefon;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Anunt{" +
                "titlu='" + titlu + '\'' +
                ", categorie='" + categorie + '\'' +
                ", descriere='" + descriere + '\'' +
                ", email='" + email + '\'' +
                ", pret='" + pret + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", nrTelefon='" + nrTelefon + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
