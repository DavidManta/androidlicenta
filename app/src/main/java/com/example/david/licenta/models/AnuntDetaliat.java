package com.example.david.licenta.models;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.licenta.R;
import com.squareup.picasso.Picasso;

/**
 * Created by David on 3/3/2018.
 */

public class AnuntDetaliat extends AppCompatActivity implements View.OnClickListener {
    private TextView titlu;
    private TextView categorie;
    private TextView descriere;
    private TextView email;
    private TextView pret;
    private TextView nrTelefon;
    private Button cancel;
    private ImageView img;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anunt_click);

        Anunt anunt = (Anunt) getIntent().getSerializableExtra("Anunt");

        titlu = (TextView) findViewById(R.id.txtTitlu);
        descriere = (TextView) findViewById(R.id.txtDescriere);
        categorie = (TextView) findViewById(R.id.txtCategorie);
        email = (TextView) findViewById(R.id.txtEmail);
        pret = (TextView) findViewById(R.id.txtPret);
        nrTelefon = (TextView) findViewById(R.id.txtTelefon);

        img=(ImageView)findViewById(R.id.ivDetaliat);

        titlu.setText(anunt.getTitlu());
        categorie.setText(anunt.getCategorie());
        descriere.setText("Descriere: " + anunt.getDescriere());
        email.setText("E-mail: " + anunt.getEmail());
        pret.setText("Pret: " + anunt.getPret() + " " + anunt.getCurrency());
        nrTelefon.setText("Numar Telefon:" + anunt.getNrTelefon());

        Picasso.with(this).load(anunt.getPictureUrl()).resize(4000, 4000).centerCrop().into(img);

        cancel = (Button) findViewById(R.id.cancelBtn);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == cancel) {
            finish();
        }
    }
}
