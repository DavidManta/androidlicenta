package com.example.david.licenta.models;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.licenta.R;
import com.google.firebase.auth.FirebaseAuth;
import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreareMesaj extends AppCompatActivity {

    private EditText destinatar;
    private EditText continut;
    private FirebaseAuth firebaseAuth;
    private Button send;
    private TextView cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compunere_mesaj);
        destinatar = (EditText) findViewById(R.id.etDestinatar);
        continut = (EditText) findViewById(R.id.etContinut);
        firebaseAuth = FirebaseAuth.getInstance();
        send = (Button) findViewById(R.id.btnSend);
        cancel=(TextView)findViewById(R.id.tvCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String catre = destinatar.getText().toString();
                String contin = continut.getText().toString();

                if (firebaseAuth.getCurrentUser() != null) {
                    if (destinatar.length() > 0 && continut.length() > 0) {

                        Date c = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String formattedDate = df.format(c);

                        Mesaj mesaj = new Mesaj(firebaseAuth.getCurrentUser().getEmail(), contin,catre , formattedDate);
                        ParseObject parseObject = new ParseObject("Mesaj");
                        parseObject.put("autor", mesaj.getAutor());
                        parseObject.put("continut", mesaj.getContinut());
                        parseObject.put("destinatar", mesaj.getDestinatar());
                        parseObject.put("data", mesaj.getDate());
                        parseObject.saveInBackground();

                        destinatar.setText(null);
                        continut.setText(null);
                        Toast.makeText(getApplicationContext(), "Mesajul a fost trimis", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "Nu ai completat toate datele", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Nu esti logat!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Mesaj mesaj = (Mesaj) getIntent().getSerializableExtra("MesajReply");
        if(mesaj!=null)
        destinatar.setText(mesaj.getAutor());
    }
}
