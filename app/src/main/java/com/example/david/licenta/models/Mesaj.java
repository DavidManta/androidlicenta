package com.example.david.licenta.models;

import java.io.Serializable;

public class Mesaj implements Serializable {
    private String autor;
    private String continut;
    private String destinatar;
    private String date;

    public Mesaj(String autor, String continut, String destinatar,String date) {
        this.autor = autor;
        this.continut = continut;
        this.destinatar = destinatar;
        this.date=date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public String getDestinatar() {
        return destinatar;
    }

    public void setDestinatar(String destinatar) {
        this.destinatar = destinatar;
    }

    @Override
    public String toString() {
        return "Mesaj{" +
                "autor='" + autor + '\'' +
                ", continut='" + continut + '\'' +
                ", destinatar='" + destinatar + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
