package com.example.david.licenta.models;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.example.david.licenta.R;

public class MesajDetaliat extends AppCompatActivity {
    private TextView autor;
    private TextView continut;
    private Button reply;
    private TextView cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_mesaj);
        autor = (TextView) findViewById(R.id.tvAutor);
        continut = (TextView) findViewById(R.id.tvContinut);
        reply = (Button) findViewById(R.id.btnReply);
        cancel = (TextView) findViewById(R.id.tvCancel);
        final Mesaj mesaj = (Mesaj) getIntent().getSerializableExtra("Mesaj");
        autor.setText("De la:" + " " + mesaj.getAutor());
        continut.setText(mesaj.getContinut());
        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MesajDetaliat.this, CreareMesaj.class);
                intent.putExtra("MesajReply", mesaj);
                startActivity(intent);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
